package org.fsd.messaging.listener;

import org.fsd.messaging.annotation.Log;
import org.fsd.messaging.event.CurrencyEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
public class RateEventListener {

	//@TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
	@TransactionalEventListener
	@Log(printParamsValues=true,callMethodWithNoParamsToString="getRate")
	public void processEvent(CurrencyEvent event){ }
}
