package org.fsd.messaging.repository;

import java.util.Date;
import java.util.List;

import org.fsd.messaging.domain.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RateRepository extends JpaRepository<Rate,String>{
List<Rate> findByDate(Date date);
Rate findByDateAndCode(Date date,String code);
}
