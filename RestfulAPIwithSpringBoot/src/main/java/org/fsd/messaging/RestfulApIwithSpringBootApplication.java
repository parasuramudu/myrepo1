package org.fsd.messaging;

import java.util.Date;

import org.fsd.messaging.domain.Rate;
import org.fsd.messaging.repository.RateRepository;
import org.fsd.messaging.service.CurrencyService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RestfulApIwithSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfulApIwithSpringBootApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner data(CurrencyService service) {
		return (args) -> {
			service.saveRate(new Rate("EUR",0.88857F,new Date()));
			service.saveRate(new Rate("JPY",102.17F,new Date()));
			service.saveRate(new Rate("MXN",19.232F,new Date()));
			service.saveRate(new Rate("GBP",0.75705F,new Date()));
		};
	}
}

